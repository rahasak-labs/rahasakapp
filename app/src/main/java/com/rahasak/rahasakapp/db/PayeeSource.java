package com.rahasak.rahasakapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rahasak.rahasakapp.pojo.Payee;

import java.util.ArrayList;

public class PayeeSource {

    public static void createPayee(Context context, Payee payee) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getWritableDatabase();

        // content values to inset
        ContentValues values = new ContentValues();
        values.put(SenzorsDbContract.Payee.COLUMN_NAME_PAYEE_NAME, payee.getName());
        values.put(SenzorsDbContract.Payee.COLUMN_NAME_BANK, payee.getBankName());
        values.put(SenzorsDbContract.Payee.COLUMN_NAME_BANK_CODE, payee.getBankCode());
        values.put(SenzorsDbContract.Payee.COLUMN_NAME_BRANCH, payee.getBranch());
        values.put(SenzorsDbContract.Payee.COLUMN_NAME_BRANCH_CODE, payee.getBranchCode());
        values.put(SenzorsDbContract.Payee.COLUMN_NAME_ACCOUNT_NO, payee.getAccountNo());

        // insert the new row, if fails throw an error
        db.insertOrThrow(SenzorsDbContract.Payee.TABLE_NAME, null, values);
    }

    public static ArrayList<Payee> getPayees(Context context) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = db.query(SenzorsDbContract.Payee.TABLE_NAME, // table
                null, // columns
                null, // constraint
                null, // params
                null, // order by
                null, // group by
                null); // join

        ArrayList<Payee> payees = new ArrayList<>();

        while (cursor.moveToNext()) {
            String username = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Payee.COLUMN_NAME_PAYEE_NAME));
            String bank = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Payee.COLUMN_NAME_BANK));
            String bankCode = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Payee.COLUMN_NAME_BANK_CODE));
            String branch = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Payee.COLUMN_NAME_BRANCH));
            String branchCode = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Payee.COLUMN_NAME_BRANCH_CODE));
            String no = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Payee.COLUMN_NAME_ACCOUNT_NO));

            Payee payee = new Payee();
            payee.setName(username);
            payee.setBankName(bank);
            payee.setBankCode(bankCode);
            payee.setBranch(branch);
            payee.setBranchCode(branchCode);
            payee.setAccountNo(no);
            payees.add(payee);
        }
        cursor.close();

        return payees;
    }

    public static boolean havePayees(Context context) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = db.query(SenzorsDbContract.Payee.TABLE_NAME, // table
                null, // columns
                null, // constraint
                null, // params
                null, // order by
                null, // group by
                null); // join

        boolean havePayees = false;
        if (cursor.moveToNext()) havePayees = true;

        cursor.close();

        return havePayees;
    }

    public static void deletePayee(Context context, Payee payee) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getWritableDatabase();

        // delete senz of given user
        db.delete(SenzorsDbContract.Payee.TABLE_NAME,
                SenzorsDbContract.Payee.COLUMN_NAME_PAYEE_NAME + " = ?",
                new String[]{payee.getName()});
    }
}
