package com.rahasak.rahasakapp.interfaces;

import com.rahasak.rahasakapp.pojo.Response;

public interface IContractExecutorListener {
    void onFinishTask(String string);
    void onFinishTask(Response response);
}
