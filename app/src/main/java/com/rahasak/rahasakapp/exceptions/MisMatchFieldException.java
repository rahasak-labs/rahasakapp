package com.rahasak.rahasakapp.exceptions;

public class MisMatchFieldException extends Exception {

    @Override
    public String toString() {
        return "password mismatch";
    }

}

