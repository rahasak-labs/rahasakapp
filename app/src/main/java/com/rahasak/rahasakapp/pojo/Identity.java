package com.rahasak.rahasakapp.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Identity implements Parcelable {
    private String did;
    private String owner;
    private String nic;
    private String name;
    private String dob;
    private String phone;
    private String email;
    private String address;
    private String taxNo;
    private String employeeName;
    private String occupation;
    private String employeeAddress;
    private String blob;
    private String password;
    private String answer1;
    private String answer2;
    private String answer3;
    private boolean verified;

    public Identity() {

    }

    protected Identity(Parcel in) {
        did = in.readString();
        owner = in.readString();
        nic = in.readString();
        name = in.readString();
        dob = in.readString();
        phone = in.readString();
        email = in.readString();
        address = in.readString();
        taxNo = in.readString();
        employeeName = in.readString();
        occupation = in.readString();
        employeeAddress = in.readString();
        blob = in.readString();
        password = in.readString();
        answer1 = in.readString();
        answer2 = in.readString();
        answer3 = in.readString();
        verified = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(did);
        dest.writeString(owner);
        dest.writeString(nic);
        dest.writeString(name);
        dest.writeString(dob);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(taxNo);
        dest.writeString(employeeName);
        dest.writeString(occupation);
        dest.writeString(employeeAddress);
        dest.writeString(blob);
        dest.writeString(password);
        dest.writeString(answer1);
        dest.writeString(answer2);
        dest.writeString(answer3);
        dest.writeByte((byte) (verified ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Identity> CREATOR = new Creator<Identity>() {
        @Override
        public Identity createFromParcel(Parcel in) {
            return new Identity(in);
        }

        @Override
        public Identity[] newArray(int size) {
            return new Identity[size];
        }
    };

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public String getBlob() {
        return blob;
    }

    public void setBlob(String blob) {
        this.blob = blob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
