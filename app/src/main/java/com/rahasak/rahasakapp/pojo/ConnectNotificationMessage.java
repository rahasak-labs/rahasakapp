package com.rahasak.rahasakapp.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class ConnectNotificationMessage implements Parcelable {
    private String traceId;
    private String tracerDid;
    private String tracerOwnerDid;
    private String tracerName;
    private String salt;

    public ConnectNotificationMessage() {
    }

    protected ConnectNotificationMessage(Parcel in) {
        traceId = in.readString();
        tracerDid = in.readString();
        tracerOwnerDid = in.readString();
        tracerName = in.readString();
        salt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(traceId);
        dest.writeString(tracerDid);
        dest.writeString(tracerOwnerDid);
        dest.writeString(tracerName);
        dest.writeString(salt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConnectNotificationMessage> CREATOR = new Creator<ConnectNotificationMessage>() {
        @Override
        public ConnectNotificationMessage createFromParcel(Parcel in) {
            return new ConnectNotificationMessage(in);
        }

        @Override
        public ConnectNotificationMessage[] newArray(int size) {
            return new ConnectNotificationMessage[size];
        }
    };

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTracerDid() {
        return tracerDid;
    }

    public void setTracerDid(String tracerDid) {
        this.tracerDid = tracerDid;
    }

    public String getTracerOwnerDid() {
        return tracerOwnerDid;
    }

    public void setTracerOwnerDid(String tracerOwnerDid) {
        this.tracerOwnerDid = tracerOwnerDid;
    }

    public String getTracerName() {
        return tracerName;
    }

    public void setTracerName(String tracerName) {
        this.tracerName = tracerName;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
