package com.rahasak.rahasakapp.pojo;

public class StatusReply {
    int code;
    String msg;

    public StatusReply() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
