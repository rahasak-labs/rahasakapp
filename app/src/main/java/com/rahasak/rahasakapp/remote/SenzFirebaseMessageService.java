package com.rahasak.rahasakapp.remote;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rahasak.rahasakapp.application.IntentProvider;
import com.rahasak.rahasakapp.pojo.ConnectNotificationMessage;
import com.rahasak.rahasakapp.util.JsonUtil;

public class SenzFirebaseMessageService extends FirebaseMessagingService {

    private static final String TAG = SenzFirebaseMessageService.class.getName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData());
            try {
                String dataMsg = remoteMessage.getData().get("message");
                ConnectNotificationMessage notificationMessage = JsonUtil.toConnectNotificationMessage(dataMsg);
                handleNotificationMessage(notificationMessage);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotificationMessage(ConnectNotificationMessage msg) {
        // broadcast message
        Intent intent = new Intent(IntentProvider.ACTION_SENZ);
        intent.putExtra("NOTIFICATION_MESSAGE", msg);
        sendBroadcast(intent);
    }

}

