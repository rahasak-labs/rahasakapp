package com.rahasak.rahasakapp.util;

import com.rahasak.rahasakapp.pojo.ConnectNotificationMessage;
import com.rahasak.rahasakapp.pojo.Identity;
import com.rahasak.rahasakapp.pojo.StatusReply;
import com.rahasak.rahasakapp.pojo.TokenReply;
import com.rahasak.rahasakapp.pojo.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class JsonUtil {

    public static String toJsonContract(HashMap<String, String> contractMap) throws JSONException {
        JSONObject jsonParam = new JSONObject(contractMap);
        return jsonParam.toString();
    }

    public static StatusReply toStatusReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            StatusReply statusReply = new StatusReply();
            statusReply.setCode(jsonObj.getInt("code"));
            statusReply.setMsg(jsonObj.getString("msg"));

            return statusReply;
        }

        throw new JSONException("Invalid JSON");
    }

    public static TokenReply toTokenReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            TokenReply tokenReply = new TokenReply();
            tokenReply.setStatus(jsonObj.getInt("status"));
            tokenReply.setToken(jsonObj.getString("token"));

            return tokenReply;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ConnectNotificationMessage toConnectNotificationMessage(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            ConnectNotificationMessage msg = new ConnectNotificationMessage();
            msg.setTraceId(jsonObj.getString("traceId"));
            msg.setTracerDid(jsonObj.getString("tracerDid"));
            msg.setTracerOwnerDid(jsonObj.getString("tracerOwnerDid"));
            msg.setTracerName(jsonObj.getString("tracerName"));
            msg.setSalt(jsonObj.getString("salt"));

            return msg;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Identity toIdentityResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            Identity identity = new Identity();
            identity.setDid(jsonObj.getString("did"));
            identity.setOwner(jsonObj.getString("owner"));
            identity.setName(jsonObj.getString("name"));
            identity.setNic(jsonObj.getString("nic"));
            identity.setDob(jsonObj.getString("dob"));
            identity.setEmail(jsonObj.getString("email"));
            identity.setPhone(jsonObj.getString("phone"));
            identity.setTaxNo(jsonObj.getString("taxNo"));
            identity.setAddress(jsonObj.getString("address"));
            identity.setBlob(jsonObj.getString("blob"));
            identity.setEmployeeName(jsonObj.getString("employeeName"));
            identity.setOccupation(jsonObj.getString("occupation"));
            identity.setEmployeeAddress(jsonObj.getString("employeeAddress"));
            identity.setVerified(jsonObj.getBoolean("verified"));

            return identity;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toActivityResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("traces");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String tracer = obj.getString("accountTracerName");
                String address = obj.getString("accountTracerAddress");
                String verified = obj.getBoolean("verified") ? "Verified" : "Unverified";
                String owner = obj.getString("accountTracerOwner");
                String time = obj.getString("timestamp");

                transaction.setId(id);
                transaction.setUser(tracer);
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setAmount(address);
                transaction.setDescription(owner);

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

}
