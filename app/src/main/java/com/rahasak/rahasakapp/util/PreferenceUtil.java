package com.rahasak.rahasakapp.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.Account;
import com.rahasak.rahasakapp.pojo.Identity;

/**
 * Utility class to deal with Share Preferences
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class PreferenceUtil {

    public static final String DID = "DID";
    public static final String OWNER = "OWNER";
    public static final String PASSWORD = "PASSWORD";

    public static final String PUBLIC_KEY = "PUBLIC_KEY";
    public static final String PRIVATE_KEY = "PRIVATE_KEY";
    public static final String ZWITCH_KEY = "ZWITCH_KEY";
    public static final String CHAINZ_KEY = "CHAINZ_KEY";

    public static final String ACCOUNT_ID = "ACCOUNT_ID";
    public static final String ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD";
    public static final String ACCOUNT_NIC = "ACCOUNT_NIC";
    public static final String ACCOUNT_NO = "ACCOUNT_NO";
    public static final String ACCOUNT_NAME = "ACCOUNT_NAME";
    public static final String ACCOUNT_PHONE = "ACCOUNT_PHONE";
    public static final String ACCOUNT_STATE = "ACCOUNT_STATE";
    public static final String QUESTION1 = "QUESTION1";
    public static final String QUESTION2 = "QUESTION2";
    public static final String QUESTION3 = "QUESTION3";
    public static final String BLOB = "BLOB";
    public static final String EMAIL = "EMAIL";
    public static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";
    public static final String UPDATE_FIREBASE_TOKEN = "UPDATE_FIREBASE_TOKEN";
    public static final String TOKEN = "TOKEN";

    public static Account getAccount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Account account = new Account();
        account.setId(preferences.getString(PreferenceUtil.ACCOUNT_ID, ""));
        account.setPassword(preferences.getString(PreferenceUtil.ACCOUNT_PASSWORD, ""));
        account.setNic(preferences.getString(PreferenceUtil.ACCOUNT_NIC, ""));
        account.setNo(preferences.getString(PreferenceUtil.ACCOUNT_NO, ""));
        account.setName(preferences.getString(PreferenceUtil.ACCOUNT_NAME, ""));
        account.setPhone(preferences.getString(PreferenceUtil.ACCOUNT_PHONE, ""));
        account.setState(preferences.getString(PreferenceUtil.ACCOUNT_STATE, "PENDING"));

        return account;
    }

    public static Identity getIdentity(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Identity identity = new Identity();
        identity.setDid(preferences.getString(PreferenceUtil.DID, ""));
        identity.setEmail(preferences.getString(PreferenceUtil.EMAIL, ""));
        identity.setPhone(preferences.getString(PreferenceUtil.ACCOUNT_PHONE, ""));
        identity.setBlob(preferences.getString(PreferenceUtil.BLOB, ""));
        identity.setName(preferences.getString(PreferenceUtil.ACCOUNT_NAME, ""));

        return identity;
    }

    public static void put(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String get(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static void put(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int get(Context context, String key, int defVal) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return preferences.getInt(key, defVal);
    }

}
