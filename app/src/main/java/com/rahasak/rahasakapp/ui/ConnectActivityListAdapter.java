package com.rahasak.rahasakapp.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.Transaction;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class ConnectActivityListAdapter extends ArrayAdapter<Transaction> {
    private Context context;
    private Typeface typeface;

    ConnectActivityListAdapter(Context _context, ArrayList<Transaction> transactionList) {
        super(_context, R.layout.connect_activity_list_row_layout, R.id.user_name, transactionList);
        context = _context;
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    /**
     * Create list row view
     *
     * @param i         index
     * @param view      current list item view
     * @param viewGroup parent
     * @return view
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        final Transaction transaction = getItem(i);

        if (view == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.connect_activity_list_row_layout, viewGroup, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.userImageView = (ImageView) view.findViewById(R.id.user_image);
            holder.selected = (ImageView) view.findViewById(R.id.selected);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.category = (TextView) view.findViewById(R.id.category);
            holder.date = (TextView) view.findViewById(R.id.date);
            holder.company = (TextView) view.findViewById(R.id.company);
            holder.dept = (TextView) view.findViewById(R.id.department);
            holder.status = (TextView) view.findViewById(R.id.status);

            view.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) view.getTag();
        }

        setUpRow(transaction, holder);

        return view;
    }

    private void setUpRow(Transaction transaction, ViewHolder viewHolder) {
        viewHolder.name.setTypeface(typeface, Typeface.BOLD);
        viewHolder.date.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.company.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.dept.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.category.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.status.setTypeface(typeface, Typeface.NORMAL);

        // set text
        viewHolder.name.setText(transaction.getUser());
        if (!transaction.getDate().isEmpty())
            viewHolder.date.setText(transaction.getDate());
        viewHolder.company.setText(transaction.getAmount());
        viewHolder.status.setText(transaction.getDescription());

        // load image
        Picasso.with(context)
                .load(R.drawable.checkdb)
                .placeholder(R.drawable.checkdb)
                .into(viewHolder.userImageView);
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    private static class ViewHolder {
        ImageView userImageView;
        ImageView selected;
        TextView name;
        TextView date;
        TextView company;
        TextView dept;
        TextView category;
        TextView status;
    }

}

