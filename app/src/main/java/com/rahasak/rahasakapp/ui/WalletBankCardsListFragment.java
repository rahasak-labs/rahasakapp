package com.rahasak.rahasakapp.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.Account;
import com.rahasak.rahasakapp.pojo.BankAccount;

import java.util.ArrayList;

public class WalletBankCardsListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ArrayList<BankAccount> accountList;
    private AccountListAdapter accountListAdapter;
    private RelativeLayout emptyView;
    private TextView balanceText;
    private TextView balanceAmountText;
    private TextView emptyText;
    private ListView accountListView;
    private LineChart chart;

    private Account account;
    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wallet_bank_cards_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initNewButton(view);
        initBalancePanel(view);
        initListView(view);
        mockList();
        //initChart(view)
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    private void initNewButton(View view) {
        // new
        FloatingActionButton newCustomer = (FloatingActionButton) view.findViewById(R.id.new_account);
        newCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private void initBalancePanel(View view) {
        balanceText = (TextView) view.findViewById(R.id.balance_title);
        balanceAmountText = (TextView) view.findViewById(R.id.balance_text);

        balanceText.setTypeface(typeface, Typeface.NORMAL);
        balanceAmountText.setTypeface(typeface, Typeface.BOLD);

        balanceAmountText.setText("Rs 34,500.00");
    }

    private void initChart(View view) {
        chart = view.findViewById(R.id.chart);
    }

    private void drawChart(ArrayList<String> promizes, ArrayList<String> transfers) {
        ArrayList<Entry> entries1 = new ArrayList<>();
        for (int i = 0; i < promizes.size(); i++) {
            entries1.add(new Entry(i + 1, Float.parseFloat(promizes.get(i))));
        }

        ArrayList<Entry> entries2 = new ArrayList<>();
        for (int i = 0; i < transfers.size(); i++) {
            entries2.add(new Entry(i + 1, Float.parseFloat(transfers.get(i))));
        }

        LineDataSet dataSet1 = new LineDataSet(entries1, "Promizes");
        dataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet1.setDrawFilled(true);
        dataSet1.setDrawValues(false);
        dataSet1.setFillColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryT));
        dataSet1.setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryT));
        dataSet1.setFillAlpha(150);
        dataSet1.setDrawCircles(false);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Transfers");
        dataSet2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet2.setDrawFilled(true);
        dataSet2.setDrawValues(false);
        dataSet2.setFillColor(ContextCompat.getColor(getActivity(), R.color.colorGreen));
        dataSet2.setColor(ContextCompat.getColor(getActivity(), R.color.colorGreen));
        dataSet2.setFillAlpha(200);
        dataSet2.setDrawCircles(false);

        // Controlling X axis
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1

        //Customizing x axis value
//        final String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep"};
//        IAxisValueFormatter formatter = new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return months[(int) value];
//            }
//        };
//        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
//        xAxis.setValueFormatter(formatter);

        // Controlling right side of y axis
        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setEnabled(false);

        // Controlling left side of y axis
        YAxis yAxisLeft = chart.getAxisLeft();
        //yAxisLeft.setGranularity(1f);
        yAxisLeft.setDrawGridLines(false);

        // Setting Data
        LineData data = new LineData();
        if (entries1.size() > 0) data.addDataSet(dataSet1);
        if (entries2.size() > 0) data.addDataSet(dataSet2);
        chart.setData(data);
        chart.animateX(2000);
        chart.getDescription().setEnabled(false);

        // refresh
        if (entries1.size() > 0 || entries2.size() > 0) {
            chart.setVisibility(View.GONE);
            chart.invalidate();
        } else {
            chart.setVisibility(View.GONE);
        }
    }

    private void initListView(View view) {
        accountListView = (ListView) view.findViewById(R.id.account_list_view);
        accountListView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);
    }

    private void mockList() {
        BankAccount acc = new BankAccount();
        acc.setNo("1111 2222 3333 4444");
        acc.setAvailableBalance("12/2022");
        acc.setCurrentBalance("Bank of America");
        acc.setName("Eranga Bandara");
        acc.setTyp("Rahasak Labs");
        acc.setPromizeAccount(true);

        BankAccount acc1 = new BankAccount();
        acc1.setNo("2222 1111 3333 4444");
        acc1.setAvailableBalance("10/2024");
        acc1.setCurrentBalance("Cahse bank");
        acc1.setName("Rahasak Labs");
        acc1.setTyp("Eranga Bandara");
        acc1.setPromizeAccount(true);

        accountList = new ArrayList<>();
        accountList.add(acc);
        accountList.add(acc1);
        //accountList.add(acc);
        //accountList.add(acc);
        if (accountList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        accountListAdapter = new AccountListAdapter(getActivity(), accountList);
        accountListView.setAdapter(accountListAdapter);
        accountListAdapter.notifyDataSetChanged();
    }

    private void refreshView(ArrayList<BankAccount> accounts) {
        // transactions
        if (accounts.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
            emptyText.setText("No accounts found. Please contact bank administrator");
        } else {
            emptyView.setVisibility(View.GONE);

            accountList = accounts;
            accountListAdapter = new AccountListAdapter(getActivity(), accountList);
            accountListView.setAdapter(accountListAdapter);
            accountListAdapter.notifyDataSetChanged();

            // show balance
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final BankAccount account = accountList.get(position);

//        Intent intent = new Intent(this.getActivity(), AccountDetailsActivity.class);
//        intent.putExtra("BANK_ACCOUNT", account);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        getActivity().overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
    }

}
