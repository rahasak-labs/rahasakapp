package com.rahasak.rahasakapp.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class WalletScanQrCodeActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private static final String TAG = WalletScanQrCodeActivity.class.getName();

    private static final int PERMISSIONS_REQUEST_CAMERA = 200;

    private ZXingScannerView scannerView;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        checkPermission();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
        } else {
            initPreview();
        }
    }

    private void initPreview() {
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkPermission();
            } else {
                displayInformationMessageDialog("Allow permission","Please allow camera permission to scan QR code");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleResult(Result result) {
        String extra = result.getText();
        Log.v(TAG, "Scan result " + extra);
        Log.v(TAG, "Scan barcode format " + result.getBarcodeFormat().toString());
    }
}
