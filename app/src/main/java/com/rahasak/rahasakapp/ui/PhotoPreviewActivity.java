package com.rahasak.rahasakapp.ui;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.Identity;
import com.rahasak.rahasakapp.util.ImageUtil;
import com.squareup.picasso.Picasso;

import java.io.File;

public class PhotoPreviewActivity extends BaseActivity {

    private FloatingActionButton cancel;
    private FloatingActionButton done;
    private ImageView imageView;

    private BottomSheetDialog bottomSheetDialog;

    private Identity identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.photo_preview_layout);

        initToolbar();
        initActionBar();
        initPrefs();
        initUi();
        loadBitmap(imageView);
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Photo");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        imageView = (ImageView) findViewById(R.id.photo_preview);
        cancel = (FloatingActionButton) findViewById(R.id.close);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        done = (FloatingActionButton) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptions();
            }
        });
    }

    private void loadBitmap(ImageView view) {
        // create bitmap based on identity blob
        if (identity != null) {
            Bitmap bitmap = ImageUtil.decodeBmp(identity.getBlob());
            imageView.setImageBitmap(bitmap);
        } else {
            // load image via picasso for test
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/rahasak/" + "ops.jpg");
            Picasso.with(this)
                    .load(file)
                    .error(R.drawable.default_user_icon)
                    .into(view);
        }
    }

    private void initBottomSheet() {
        View view = getLayoutInflater().inflate(R.layout.identity_info_view_layout, null);
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                View bottomSheetInternal = d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView nic = (TextView) view.findViewById(R.id.nic);
        TextView nicv = (TextView) view.findViewById(R.id.nicv);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView namev = (TextView) view.findViewById(R.id.namev);
        TextView phone = (TextView) view.findViewById(R.id.phone);
        TextView phonev = (TextView) view.findViewById(R.id.phonev);
        TextView email = (TextView) view.findViewById(R.id.email);
        TextView emailv = (TextView) view.findViewById(R.id.emailv);
        TextView tax = (TextView) view.findViewById(R.id.tax);
        TextView taxv = (TextView) view.findViewById(R.id.taxv);
        TextView employee = (TextView) view.findViewById(R.id.employee);
        TextView employeev = (TextView) view.findViewById(R.id.employeev);
        TextView occupation = (TextView) view.findViewById(R.id.occupation);
        TextView occupationv = (TextView) view.findViewById(R.id.occupationv);
        TextView address = (TextView) view.findViewById(R.id.address);
        TextView addressv = (TextView) view.findViewById(R.id.addressv);
        TextView status = (TextView) view.findViewById(R.id.status);
        TextView statusv = (TextView) view.findViewById(R.id.statusv);
        TextView options = (TextView) view.findViewById(R.id.options);

        // set font
        nic.setTypeface(typeface, Typeface.NORMAL);
        nicv.setTypeface(typeface, Typeface.NORMAL);
        name.setTypeface(typeface, Typeface.NORMAL);
        namev.setTypeface(typeface, Typeface.NORMAL);
        phone.setTypeface(typeface, Typeface.NORMAL);
        phonev.setTypeface(typeface, Typeface.NORMAL);
        email.setTypeface(typeface, Typeface.NORMAL);
        emailv.setTypeface(typeface, Typeface.NORMAL);
        tax.setTypeface(typeface, Typeface.NORMAL);
        taxv.setTypeface(typeface, Typeface.NORMAL);
        employee.setTypeface(typeface, Typeface.NORMAL);
        employeev.setTypeface(typeface, Typeface.NORMAL);
        occupation.setTypeface(typeface, Typeface.NORMAL);
        occupationv.setTypeface(typeface, Typeface.NORMAL);
        address.setTypeface(typeface, Typeface.NORMAL);
        addressv.setTypeface(typeface, Typeface.NORMAL);
        status.setTypeface(typeface, Typeface.NORMAL);
        statusv.setTypeface(typeface, Typeface.NORMAL);
        options.setTypeface(typeface, Typeface.NORMAL);

        nicv.setText(identity.getNic());
        namev.setText(identity.getName());
        phonev.setText(identity.getPhone());
        emailv.setText(identity.getEmail());
        taxv.setText(identity.getTaxNo());
        employeev.setText(identity.getEmployeeName());
        occupationv.setText(identity.getOccupation());
        addressv.setText(identity.getAddress());
        statusv.setText(identity.isVerified() ? "Verified" : "Pending");

        // set signature test
//        StringBuilder signatureText = new StringBuilder();
//        for (String signer : document.getSigners().split(",")) {
//            // fruit is an element of the `fruits` array.
//            String signed = document.getSignatureMap().get(signer) != null ? document.getSignatureMap().get(signer).getStatus() : "Pending";
//            signatureText.append(signer.trim()).append(" - ").append(signed).append("\n\n");
//        }
//        signersv.setText(signatureText.toString().trim());

        Button approveBtn = view.findViewById(R.id.accept_btn);
        approveBtn.setTypeface(typeface);
        approveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approveIdentity();
            }
        });
    }

    private void showOptions() {
        initBottomSheet();
        bottomSheetDialog.show();
    }

    private void approveIdentity() {

    }

}
