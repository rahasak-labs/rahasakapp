package com.rahasak.rahasakapp.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.application.SenzApplication;
import com.rahasak.rahasakapp.util.RootUtil;

public class SplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.splash_layout);

        initUi();
        initApp();
    }

    private void initApp() {
        if (RootUtil.isDeviceRooted()) {
            displayInformationMessageDialogConfirm("Rooted device", "Application not allowed to install on rooted devices", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        } else {
            initNavigation();
        }
    }

    private void initUi() {
        ((TextView) findViewById(R.id.splash_name)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.desc)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.powerd)).setTypeface(typeface, Typeface.BOLD);

        // set status bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

    private void initNavigation() {
        if (SenzApplication.isLogin()) {
            navigateToHome();
        } else {
            // stay 3 seconds in splash
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigateToMessageList();
                    //navigateToHome();
                }
            }, 3000);
        }
    }

    public void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void navigateToMessageList() {
        Intent intent = new Intent(this, ChatMessageListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

}