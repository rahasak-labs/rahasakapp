package com.rahasak.rahasakapp.ui;

import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.util.concurrent.ListenableFuture;
import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.ChatMessage;
import com.rahasaklabs.messanger.spec.MessageServiceGrpc;
import com.rahasaklabs.messanger.spec.MessangerProto;

import java.util.ArrayList;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class ChatMessageListActivity extends AppCompatActivity {

    private static final String TAG = ChatMessageListActivity.class.getName();

    private EditText txtMessage;
    private TextView btnSend;
    private Typeface typeface;

    private ListView listView;
    private ChatMessangerListAdapter adapter;
    private ArrayList<ChatMessage> chatMessageList;

    private ManagedChannel channel;
    private MessageServiceGrpc.MessageServiceStub asyncStub;
    private MessageServiceGrpc.MessageServiceFutureStub futureStub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.message_list_activity);

        initUi();
        setupToolbar();
        setupActionBar();
        initMessageList();
        initGrpcComm();
    }

    private void initUi() {
        typeface = Typeface.createFromAsset(getAssets(), "fonts/GeosansLight.ttf");

        // init message view
        txtMessage = (EditText) findViewById(R.id.text_message);
        txtMessage.setTypeface(typeface, Typeface.NORMAL);
        txtMessage.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        // init send button
        btnSend = (TextView) findViewById(R.id.sendBtn);
        btnSend.setTypeface(typeface, Typeface.BOLD);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSend();
            }
        });
    }

    private void initMessageList() {
        // init list view
        listView = (ListView) findViewById(R.id.messages_list_view);
        listView.setDivider(null);
        listView.setDividerHeight(0);

        chatMessageList = new ArrayList<>();
        adapter = new ChatMessangerListAdapter(this, chatMessageList);
        listView.setAdapter(adapter);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.chat_activity_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        TextView header = ((TextView) findViewById(R.id.user_name));
        header.setTypeface(typeface, Typeface.BOLD);
        header.setText("@rahasaklabs");

        ImageView btnBack = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.back_btn);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ImageView btnUserSetting = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.user_profile_image);
        btnUserSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void onClickSend() {
        String msg = txtMessage.getText().toString().trim();
        if (!msg.isEmpty()) {
            // clear text
            txtMessage.setText("");

            ChatMessage chatMessage = new ChatMessage();
            Long timestamp = System.currentTimeMillis() / 1000;
            chatMessage.setId(timestamp.toString());
            chatMessage.setBody(msg);
            chatMessage.setIsMyMessage(true);
            chatMessage.setTimestamp(timestamp);
            addMessageToList(chatMessage);

            // send message via grpc
            grpcServeMessage(chatMessage);
        }
    }

    private void addMessageToList(ChatMessage chatMessage) {
        chatMessageList.add(chatMessage);
        adapter.notifyDataSetChanged();
//        listView.post(new Runnable() {
//            public void run() {
//                listView.smoothScrollToPosition(listView.getCount() - 1);
//            }
//        });
    }

    private void initGrpcComm() {
        channel = ManagedChannelBuilder
                .forAddress("10.252.94.210", 9000)
                .usePlaintext()
                .build();
        futureStub = MessageServiceGrpc.newFutureStub(channel);
        asyncStub = MessageServiceGrpc.newStub(channel);
    }

    private void grpcSendMessage(ChatMessage chatMessage) {
        MessangerProto.Message protoMessage = MessangerProto.Message.newBuilder()
                .setId(chatMessage.getId())
                .setBody(chatMessage.getBody())
                .build();

        ListenableFuture<MessangerProto.Reply> reply = futureStub.sendMessage(protoMessage);
    }

    private void grpcServeMessage(ChatMessage chatMessage) {
        // response observer
        StreamObserver responseObserver = new StreamObserver<MessangerProto.Message>() {
            @Override
            public void onNext(MessangerProto.Message protoMessage) {
                Log.d(TAG, protoMessage.getBody());

                // construct ChatMessage from the protobuf message
                final ChatMessage recv = new ChatMessage();
                Long timestamp = System.currentTimeMillis() / 1000;
                recv.setId(protoMessage.getId());
                recv.setBody(protoMessage.getBody());
                recv.setTimestamp(timestamp);
                recv.setIsMyMessage(false);

                // populate chat list with the received message
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addMessageToList(recv);
                    }
                });
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
            }

            @Override
            public void onCompleted() {

            }
        };

        // send message
        MessangerProto.Message protoMessage = MessangerProto.Message.newBuilder()
                .setId(chatMessage.getId())
                .setBody(chatMessage.getBody())
                .build();

        // server server stream with response observer
        asyncStub.serveMessage(protoMessage, responseObserver);
    }

    private void grpcStreamMessage() {
        // response observer
        StreamObserver responseObserver = new StreamObserver<MessangerProto.Message>() {
            @Override
            public void onNext(MessangerProto.Message value) {
                Log.d(TAG, value.getBody());
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {

            }
        };

        // server server stream with response observer
        StreamObserver requestObserver = asyncStub.streamMessage(responseObserver);
    }

}
