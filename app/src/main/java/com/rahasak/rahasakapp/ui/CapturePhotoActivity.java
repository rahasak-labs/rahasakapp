package com.rahasak.rahasakapp.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.Identity;
import com.rahasak.rahasakapp.util.ActivityUtil;
import com.rahasak.rahasakapp.util.ImageUtil;

public class CapturePhotoActivity extends BaseActivity {

    // camera
    private Camera camera;
    private CameraPreview cameraPreview;
    private boolean isCameraOn;

    // img panel
    private FrameLayout previewLayout;
    private FrameLayout overlay;
    private ImageView capturedPhoto;

    // signature
    private RelativeLayout signatureContainer;
    private RelativeLayout signatureLayout;
    private TextView signatureText;

    // buttons
    private FloatingActionButton capture;
    private FloatingActionButton send;
    private ImageView addPhoto;

    private Identity identity;

    private PowerManager.WakeLock wakeLock;

    private static final int PERMISSIONS_REQUEST_CAMERA = 200;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capture_photo_activity_layout);

        // init
        initPrefs();
        initUi();
        initSignature();
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();

        releaseWakeLock();
        releaseCameraPreview();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
        } else {
            acquireWakeLock();
            startPreview();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkPermission();
            } else {
                displayInformationMessageDialog("Allow permission", "Please allow camera permission to scan QR code");
            }
        }
    }

    private void initUi() {
        previewLayout = (FrameLayout) findViewById(R.id.preview_frame);
        overlay = (FrameLayout) findViewById(R.id.overlay_frame);
        capturedPhoto = (ImageView) findViewById(R.id.captured_photo);

        signatureContainer = (RelativeLayout) findViewById(R.id.signature_container);
        signatureLayout = (RelativeLayout) findViewById(R.id.signature_layout);
        signatureText = (TextView) findViewById(R.id.signature_text);
        signatureText.setTypeface(typeface, Typeface.BOLD);

        capture = (FloatingActionButton) findViewById(R.id.capture);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtil.hideSoftKeyboard(CapturePhotoActivity.this);
                onClickCapture();
            }
        });

        addPhoto = (ImageView) findViewById(R.id.add_photo);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPreview();
            }
        });

        send = (FloatingActionButton) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNext();
                //navigateHome();
            }
        });

    }

    private void initSignature() {
        Signature signature = new Signature(this, null);
        signatureContainer.addView(signature);
    }

    private void startPreview() {
        // init camera with front
        addBackground(R.color.colorPrimaryTrans);
        isCameraOn = true;
        initCameraPreview(Camera.CameraInfo.CAMERA_FACING_FRONT);

        capture.setVisibility(View.VISIBLE);
        send.setVisibility(View.GONE);
        addPhoto.setVisibility(View.GONE);

        signatureLayout.setVisibility(View.GONE);
    }

    private void addBackground(int color) {
        overlay.setBackgroundColor(getResources().getColor(color));
        capturedPhoto.setVisibility(View.GONE);
    }

    private void acquireWakeLock() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SenzWakeLock");
        wakeLock.acquire();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void releaseWakeLock() {
        wakeLock.release();

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initCameraPreview(int camFace) {
        if (isCameraOn) {
            try {
                camera = Camera.open(camFace);
                cameraPreview = new CameraPreview(this, camera, camFace);
                previewLayout.addView(cameraPreview);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void releaseCameraPreview() {
        if (isCameraOn) {
            try {
                if (camera != null) {
                    cameraPreview.surfaceDestroyed(cameraPreview.getHolder());
                    cameraPreview.getHolder().removeCallback(cameraPreview);
                    cameraPreview.destroyDrawingCache();
                    previewLayout.removeView(cameraPreview);

                    camera.stopPreview();
                    camera.release();
                    camera = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onClickCapture() {
        // AudioUtil.shootSound(this);
        camera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes, Camera camera) {
                // resize
                byte[] resizedImage = ImageUtil.compressImg(bytes, true, true);
                releaseCameraPreview();
                isCameraOn = false;

                // set image to bg
                capturedPhoto.setVisibility(View.VISIBLE);
                Bitmap bitmap = ImageUtil.bytesToBmp(resizedImage);
                capturedPhoto.setImageBitmap(bitmap);

                capture.setVisibility(View.GONE);
                addPhoto.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);

                //signature
                signatureLayout.setVisibility(View.VISIBLE);
                Animation a = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_in);
                signatureLayout.startAnimation(a);
            }
        });
    }

    private void captureScreen() {
        // create bitmap screen capture
        View v1 = findViewById(R.id.capture_frame);
        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

        byte[] bmpByte = ImageUtil.compressImg(ImageUtil.bmpToBytes(bitmap), false, false);
        String imgBlob = ImageUtil.encodeBmp(bmpByte);
        identity.setBlob(imgBlob);
    }

    private void saveImage() {
        // save image
        String imgName = "ops" + ".jpg";
        ImageUtil.saveImg(imgName, identity.getBlob());
    }

    public class Signature extends View {
        static final float STROKE_WIDTH = 2f;
        static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        Paint paint = new Paint();
        Path path = new Path();

        float lastTouchX;
        float lastTouchY;
        final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    private void onClickNext() {
        captureScreen();
        saveImage();
    }

}