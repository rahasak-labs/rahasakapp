package com.rahasak.rahasakapp.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.application.SenzApplication;
import com.rahasak.rahasakapp.async.ContractExecutor;
import com.rahasak.rahasakapp.interfaces.IContractExecutorListener;
import com.rahasak.rahasakapp.pojo.Account;
import com.rahasak.rahasakapp.pojo.Response;
import com.rahasak.rahasakapp.pojo.Transaction;
import com.rahasak.rahasakapp.util.ActivityUtil;
import com.rahasak.rahasakapp.util.JsonUtil;
import com.rahasak.rahasakapp.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class WalletBixcoinListFragment extends Fragment implements AdapterView.OnItemClickListener, IContractExecutorListener {

    private ArrayList<Transaction> promizeList;
    private ConnectActivityListAdapter adapter;
    private ListView listView;
    private RelativeLayout emptyView;
    private TextView emptyText;
    private Account account;

    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wallet_transaction_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initActionButtons(view);
        initListView(view);
        //fetchActivities("0", "20");

        mockList();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (SenzApplication.isRefreshPromize()) {
            fetchActivities("0", "20");
        }
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (!hidden) {
//            if (SenzApplication.isRefreshPromize()) {
//                fetchTrans("0", "20");
//            }
//        }
//    }

    private void initListView(View view) {
        listView = (ListView) view.findViewById(R.id.cheque_list_view);
        listView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);

        promizeList = new ArrayList<>();
        adapter = new ConnectActivityListAdapter(getActivity(), promizeList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void initActionButtons(View view) {
        FloatingActionButton sendPromize = (FloatingActionButton) view.findViewById(R.id.send_promize);
        sendPromize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        FloatingActionButton receivePromize = (FloatingActionButton) view.findViewById(R.id.receive_promize);
        receivePromize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    public void askAmount() {
        final Dialog dialog = new Dialog(getActivity());

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_amount_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        // texts
        TextView title = (TextView) dialog.findViewById(R.id.title);
        final EditText amountText = (EditText) dialog.findViewById(R.id.amount);
        title.setTypeface(typeface, Typeface.BOLD);
        amountText.setTypeface(typeface, Typeface.NORMAL);

        // set ok button
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setTypeface(typeface, Typeface.BOLD);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amount = amountText.getText().toString().trim();
                // TODO validate amount

                if (!amount.isEmpty()) {
                    Intent intent = new Intent(getActivity(), WalletQrCodeActivity.class);
                    intent.putExtra("EXTRA", amount);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);

                    dialog.cancel();
                } else {
                    Toast.makeText(getActivity(), "Invalid amount", Toast.LENGTH_LONG).show();
                }
            }
        });

        // cancel button
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setTypeface(typeface, Typeface.BOLD);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void refreshView(ArrayList<Transaction> list) {
        // transactions
        //promizeList.addAll(list);
        promizeList = list;
        if (promizeList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            listView.setEmptyView(emptyView);
            emptyText.setText("No activity trace records found with your account.");
        } else {
            emptyView.setVisibility(View.GONE);
            adapter = new ConnectActivityListAdapter(getActivity(), promizeList);
            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
        }
    }

    private void fetchActivities(String offset, String limit) {
        if (SenzApplication.isLogin()) {
            try {
                account = PreferenceUtil.getAccount(this.getActivity());
                String did = PreferenceUtil.get(getActivity(), PreferenceUtil.DID);
                String owner = PreferenceUtil.get(getActivity(), PreferenceUtil.OWNER);

                HashMap<String, String> createMap = new HashMap<>();
                createMap.put("id", account.getId() + System.currentTimeMillis());
                createMap.put("execer", account.getId());
                createMap.put("messageType", "searchTrace");
                createMap.put("did", did);
                createMap.put("owner", owner);
                createMap.put("nameTerm", "");
                createMap.put("idTerm", "");
                createMap.put("didTerm", "");
                createMap.put("nic", "");
                createMap.put("phone", "");
                createMap.put("offset", offset);
                createMap.put("limit", limit);
                createMap.put("sort", "descending");

                SenzApplication.setRefreshPromize(false);

                ActivityUtil.showProgressDialog(getActivity(), "Fetching activities...");
                ContractExecutor task = new ContractExecutor(createMap, this);
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, ContractExecutor.TRACE_API, PreferenceUtil.get(getActivity(), PreferenceUtil.TOKEN));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Transaction transaction = promizeList.get(position);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response != null && response.getStatus() == 200) {
                ArrayList<Transaction> list = JsonUtil.toActivityResponse(response.getPayload());
                refreshView(list);
                SenzApplication.setRefreshPromize(false);
            } else {
                ActivityUtil.cancelProgressDialog();
                Toast.makeText(getActivity(), "Fail to fetch activities", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Fail to fetch activities", Toast.LENGTH_LONG).show();
        }
    }

    private void mockList() {
        // add sample list items
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            final Transaction transaction1 = new Transaction();
            transaction1.setUser("Sampath bank");
            transaction1.setAmount("2300.00");
            transaction1.setDate("2020/06/03");
            transaction1.setDescription("Credit");

            final Transaction transaction2 = new Transaction();
            transaction2.setUser("Coffee Labs");
            transaction2.setAmount("3400.00");
            transaction2.setDate("2020/06/02");
            transaction2.setDescription("Debit");

            transactions.add(transaction1);
            transactions.add(transaction2);
        }

        refreshView(transactions);
    }
}
