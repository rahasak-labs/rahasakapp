package com.rahasak.rahasakapp.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.rahasakapp.R;


public class HomeActivity extends BaseActivity {

    private TextView titleTextView;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.home_layout);
        initToolbar();
        initActionBar();
        initBottomFragment();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        titleTextView = (TextView) findViewById(R.id.title);
        titleTextView.setTypeface(typeface, Typeface.BOLD);
        titleTextView.setText("Bixcoin");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initBottomFragment() {
        // first show wallet
        changeFragment(new WalletBixcoinListFragment(), WalletBixcoinListFragment.class.getSimpleName());

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigation_offers:
                                titleTextView.setText("Bixcoin");
                                changeFragment(new WalletBixcoinListFragment(), WalletBixcoinListFragment.class.getSimpleName());
                                break;
                            case R.id.navigation_promize:
                                titleTextView.setText("Bitcoin");
                                changeFragment(new WalletBitcoinListFragment(), WalletBitcoinListFragment.class.getSimpleName());
                                break;
                            case R.id.navigation_fund_transfer:
                                titleTextView.setText("Bank Cards");
                                changeFragment(new WalletBankCardsListFragment(), WalletBankCardsListFragment.class.getSimpleName());
                                break;
                            case R.id.navigation_payment:
                                titleTextView.setText("Payments");
                                break;
                            case R.id.navigation_settings:
                                titleTextView.setText("Preference");
                                changeFragment(new WalletPreferenceFragment(), WalletPreferenceFragment.class.getSimpleName());
                                break;
                        }
                        return true;
                    }
                });
    }

    public void changeFragment(Fragment fragment, String tagFragmentName) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frame_layout, fragmentTemp, tagFragmentName);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        displayConfirmationMessageDialog("Logout", "Are you sure to exit RahasakApp", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

}
