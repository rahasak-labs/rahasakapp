package com.rahasak.rahasakapp.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.rahasakapp.R;
import com.rahasak.rahasakapp.pojo.ChatMessage;
import com.rahasak.rahasakapp.util.TimeUtil;

import java.util.ArrayList;

class ChatMessangerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ChatMessage> chatMessageList;

    private Typeface typeface;

    private static final int MY_CHAT_ITEM = 0;
    private static final int FRIEND_CHAT_ITEM = 1;
    private static final int MAX_TYPE_COUNT = 2;

    ChatMessangerListAdapter(Context context, ArrayList<ChatMessage> chatMessageList) {
        this.context = context;
        this.chatMessageList = chatMessageList;

        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    @Override
    public int getCount() {
        return chatMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ((ChatMessage) getItem(position)).getIsMyMessage() ? MY_CHAT_ITEM : FRIEND_CHAT_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return MAX_TYPE_COUNT;
    }

    @Override
    public Object getItem(int position) {
        return chatMessageList.get(position);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        final ChatMessage chatMessage = chatMessageList.get(position);
        final int type = getItemViewType(position);

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = new ViewHolder();

            switch (type) {
                case MY_CHAT_ITEM:
                    view = layoutInflater.inflate(R.layout.my_chat_view_row_layout, parent, false);
                    holder.chatCamHolder = (RelativeLayout) view.findViewById(R.id.chat_cam_holder);
                    holder.chatMisHolder = (RelativeLayout) view.findViewById(R.id.chat_mis_holder);
                    holder.chatMsgHolder = (LinearLayout) view.findViewById(R.id.chat_msg_holder);

                    holder.chatCam = (ImageView) view.findViewById(R.id.chat_cam);
                    holder.chatMsg = (TextView) view.findViewById(R.id.chat_msg);
                    holder.chatMis = (RelativeLayout) view.findViewById(R.id.chat_mis);
                    holder.missedSecret = (TextView) view.findViewById(R.id.missed_secret);

                    holder.chatStatus = (FrameLayout) view.findViewById(R.id.chat_status);
                    holder.chatTime = (TextView) view.findViewById(R.id.chat_time);
                    holder.chatDelivered = (ImageView) view.findViewById(R.id.chat_delivered);
                    holder.chatPending = (ImageView) view.findViewById(R.id.chat_pending);

                    break;
                case FRIEND_CHAT_ITEM:
                    view = layoutInflater.inflate(R.layout.friend_chat_view_row_layout, parent, false);
                    holder.chatCamHolder = (RelativeLayout) view.findViewById(R.id.chat_cam_holder);
                    holder.chatMisHolder = (RelativeLayout) view.findViewById(R.id.chat_mis_holder);
                    holder.chatMsgHolder = (LinearLayout) view.findViewById(R.id.chat_msg_holder);

                    holder.chatCam = (ImageView) view.findViewById(R.id.chat_cam);
                    holder.chatMsg = (TextView) view.findViewById(R.id.chat_msg);
                    holder.chatMis = (RelativeLayout) view.findViewById(R.id.chat_mis);
                    holder.missedSecret = (TextView) view.findViewById(R.id.missed_secret);

                    holder.chatStatus = (FrameLayout) view.findViewById(R.id.chat_status);
                    holder.chatTime = (TextView) view.findViewById(R.id.chat_time);
                    holder.chatDelivered = (ImageView) view.findViewById(R.id.chat_delivered);
                    holder.chatPending = (ImageView) view.findViewById(R.id.chat_pending);

                    break;
            }

            holder.chatMsg.setTypeface(typeface, Typeface.BOLD);
            holder.chatTime.setTypeface(typeface);
            holder.missedSecret.setTypeface(typeface, Typeface.BOLD);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        setupRow(chatMessage, holder);
        return view;
    }

    private void setupRow(final ChatMessage chatMessage, ViewHolder holder) {
        holder.chatMsg.setText(chatMessage.getBody());

        if (chatMessage.getTimestamp() != null) {
            holder.chatTime.setText(TimeUtil.getTimeInWords(chatMessage.getTimestamp()));
        }
    }

    private static class ViewHolder {
        RelativeLayout chatCamHolder;
        RelativeLayout chatMisHolder;
        LinearLayout chatMsgHolder;

        ImageView chatCam;
        TextView chatMsg;
        RelativeLayout chatMis;
        TextView missedSecret;

        FrameLayout chatStatus;
        TextView chatTime;
        ImageView chatDelivered;
        ImageView chatPending;
    }

}
